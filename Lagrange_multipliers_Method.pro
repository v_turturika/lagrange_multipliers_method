#-------------------------------------------------
#
# Project created by QtCreator 2015-10-20T20:32:59
#
#-------------------------------------------------

QT       += core gui
CONFIG += c++14

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Lagrange_multipliers_Method
TEMPLATE = app


SOURCES += main.cpp\
        window.cpp\
        LagrangeMult.cpp

HEADERS  += window.h\
         LagrangeMult.h

FORMS    += window.ui

RESOURCES += \
    resources.qrc
