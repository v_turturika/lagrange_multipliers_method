#include "LagrangeMult.h"
#include <algorithm>
#include <stdexcept>

//root constructors and methods

root::root() {

	lambda = x = y = 0;
}

root::root(double lambda, double x, double y) {
	
	this-> lambda = lambda;
	this-> x = x;
	this-> y = y;
}

std::ostream& operator<<(std::ostream& out, const root z) {

	out << z.lambda << ", " << z.x << ", " << z.y;

	return out;
}

// LangrangeMult methods implementation

const int LagrangeMult::SIZE = 2;

LagrangeMult::LagrangeMult() {

	a = b = c = maxF = minF = 0;
	hasCalculated = false;
	for (int i = 0; i < SIZE; i++) {
		
		hessians.push_back(matrix());

		minors.push_back(vector<double>());
		minors[i].insert(minors[i].begin(), SIZE, 0);

		rootOf.push_back( pair<root,extremum>() );
		rootOf[rootOf.size() - 1].second = INFLECTION;
	}
	
}

LagrangeMult::LagrangeMult(double a, double b, double c) {

	this->a = a;
	this->b = b;
	this->c = c;
	maxF = minF = 0;
	hasCalculated = false;

	for (int i = 0; i < SIZE; i++) {

		hessians.push_back(matrix());

		minors.push_back(vector<double>());
		minors[i].insert(minors[i].begin(), SIZE, 0);

		rootOf.push_back(pair<root, extremum>());
		rootOf[rootOf.size() - 1].second = INFLECTION;
	}

}

// LagrangeMult get methods;

double LagrangeMult::getA() const {	
	return a;
}

double LagrangeMult::getB() const {
	return b;
}

double LagrangeMult::getC() const { 
	return c;
}

double LagrangeMult::getMinor(int i, int j) {

	if (!hasCalculated) solve();
	if ((i >= 0) && (i < SIZE) && (j >= 0) && (j < SIZE)) {
		return minors[i][j];
	}
	else
		throw out_of_range("Out of range in getMinor method");
}

const root LagrangeMult::getRoot(int i) {
	
	if (!hasCalculated) solve();
	if ((i >= 0) && (i < SIZE))
		return rootOf[i].first;
	else
		throw out_of_range("Out of range in getRoot method");
}

const matrix & LagrangeMult::getHessian(int i) {

	if (!hasCalculated) solve();
	if ((i >= 0) && (i < SIZE))
		return hessians[i];
	else
		throw out_of_range("Out of range in getHessian method");
}

const extremum LagrangeMult::getExtremum(int i) {

	if (!hasCalculated) solve();

	if ((i >= 0) && (i < SIZE)) {
		return rootOf[i].second;
	}
	else
		throw out_of_range("Out of range in getExtremum method");
}

double LagrangeMult::getMaximum() {

	if (!hasCalculated) solve();
	return maxF;
}

double LagrangeMult::getMinimum() {
	
	if (!hasCalculated) solve();
	return minF;
}

//LagrangeMult set methods

void LagrangeMult::setA(double a) {

	this->a = a;
	hasCalculated = false;
}

void LagrangeMult::setB(double b) {

	this->b = b;
	hasCalculated = false;
}

void LagrangeMult::setC(double c) {

	this->c = c;
	hasCalculated = false;
}

void LagrangeMult::setCoefficients(double a, double b, double c) {

	this->a = a;
	this->b = b;
	this->c = c;
	hasCalculated = false;
}

//LagrangeMult private methods

void LagrangeMult::solveSystem() {

	double lambda, x, y;

	lambda = (-a*c - 4 * b + sqrt(a*a*c*c - 8 * a*b*c + 16 * b*b + 16 * c)) / (-8.0*c);
	y = -3.0*(a - 4 * lambda) / sqrt(a*a*c - 8 * a*c*lambda + 16 * c*lambda*lambda + 4);
	x = -y / (a - 4.0*lambda);

	rootOf[0].first = root(lambda, x, y);

	lambda = (a*c + 4 * b + sqrt(a*a*c*c - 8 * a*b*c + 16 * b*b + 16 * c)) / (8.0*c);
	y = -3.0*(a - 4 * lambda) / sqrt(a*a*c - 8 * a*c*lambda + 16 * c*lambda*lambda + 4);
	x = -y / (a - 4.0*lambda);

	rootOf[1].first = root(lambda, x, y);
}

double LagrangeMult::det(matrix m) const {

	if (m.size() == 2) {
		
		return m[0][0] * m[1][1] - m[0][1] * m[1][0];
	}

	else if (m.size() == 3) {

		return m[0][0]*m[1][1]*m[2][2] + m[1][0]*m[2][1]*m[0][2] + m[0][1]*m[1][2]*m[2][0]
			 - m[0][2]*m[1][1]*m[2][0] - m[1][0]*m[0][1]*m[2][2] - m[2][1]*m[1][2]*m[0][0];
	}

}

matrix LagrangeMult::initHessian(const root r)
{
	matrix result;

	for (int i = 0; i < SIZE+1; i++) {

		result.push_back(vector<double>());
		result[i].insert(result[i].begin(), SIZE + 1, 0);	
	}

	result[0][1] = result[1][0] = -8 * r.x;
	result[0][2] = result[2][0] = -2 * c*r.y;
	result[1][2] = result[2][1] = 2;
	result[1][1] = 2 * a - 8 * r.lambda;
	result[2][2] = -2 * c*r.lambda + 2 * b;

	return result;
}

double LagrangeMult::calcMinore(matrix& m, bool mustReducedOrder) {

	if ( mustReducedOrder ) {
		
		matrix temp = m;

		temp.erase(temp.begin() + temp.size() - 1);
		for (int i = 0; i < SIZE; i++) {

			temp[i].erase(temp[i].begin() + temp[i].size() - 1);
		}

		return det(temp);
	}

	else {
		return det(m);
	}
}

void LagrangeMult::checkExtremum(int i) {
	
    rootOf[i].second = INFLECTION;
	//check max
	if (minors[i][0] <= 0 && minors[i][1] >= 0) {
		
		maxF = 	  a * rootOf[i].first.x * rootOf[i].first.x 
				+ 2 * rootOf[i].first.x * rootOf[i].first.y
				+ b * rootOf[i].first.y * rootOf[i].first.y;

		rootOf[i].second = MAX;
	}
	//check min
	else if (minors[i][0] < 0 && minors[i][1] < 0) {

		minF =	  a * rootOf[i].first.x * rootOf[i].first.x
				+ 2 * rootOf[i].first.x * rootOf[i].first.y
				+ b * rootOf[i].first.y * rootOf[i].first.y;

		rootOf[i].second = MIN;
	}

}

void LagrangeMult::solve() {

	hasCalculated = true;
	solveSystem();

	for (int i = 0; i < SIZE; i++) 
		hessians[i] = initHessian( getRoot(i) );

	for (int i = 0; i < SIZE; i++) {
		
		for (int j = 0; j < SIZE; j++) {

			minors[i][j] = calcMinore(hessians[i], j == 0 ? true : false);
		}
		checkExtremum(i);
	}
	
}
