#ifndef WINDOW_H
#define WINDOW_H

#include <QMainWindow>
#include <QMessageBox>

#include "LagrangeMult.h"

namespace Ui {
class Window;
}

class Window : public QMainWindow
{
    Q_OBJECT

public:
    explicit Window(QWidget *parent = 0);
    ~Window();

private slots:
    void on_pushButtonSolve_clicked();

    void on_comboBoxRoot_currentIndexChanged(int index);

private:
    Ui::Window *ui;
    LagrangeMult lm;
    const int NUM_OF_ROOT;
};

#endif // WINDOW_H
