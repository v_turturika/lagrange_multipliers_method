#include "window.h"
#include "ui_window.h"

Window::Window(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Window),
    NUM_OF_ROOT(2)
{
    ui->setupUi(this);

}

Window::~Window()
{
    delete ui;
}

void Window::on_pushButtonSolve_clicked()
{
    bool okA, okB, okC;
    okA = okB = okC = true;

    //check a, b, c input
    ui->lineEditA->text().toDouble(&okA);
    ui->lineEditB->text().toDouble(&okB);
    ui->lineEditC->text().toDouble(&okC);

    if(okA && okB && okC) {

        lm.setA( ui->lineEditA->text().toDouble() );
        lm.setB( ui->lineEditB->text().toDouble() );
        lm.setC( ui->lineEditC->text().toDouble() );
    }

    else {
        QMessageBox msg;
        msg.setText("Помилка!");
        msg.setInformativeText("Перевірте коректність введених даних");
        msg.addButton(QMessageBox::Ok);
        msg.exec();

        ui->lineEditA->setText("");
        ui->lineEditB->setText("");
        ui->lineEditC->setText("");

        return;
    }

    lm.getRoot(0); //solving problem;

    ui->comboBoxRoot->clear();
    ui->tableHessian->clear();

    for(int i=0; i<NUM_OF_ROOT; i++) { //adding number of roots to comboBox

        ui->comboBoxRoot->addItem(QString::number(i+1));
    }
    ui->comboBoxRoot->setCurrentIndex(0);

}

void Window::on_comboBoxRoot_currentIndexChanged(int index)
{
    if(index == -1) return;

    //showing root[index] : lambda, x, y
    ui->lineEditLambda -> setText(QString::number( lm.getRoot(index).lambda ));
    ui->lineEditX    ->   setText(QString::number( lm.getRoot(index).x ));
    ui->lineEditY    ->   setText(QString::number( lm.getRoot(index).y ));

    //showing minor[index,0] and minor[index,1] (2, 3 orders)
    ui->lineEditDelta1 -> setText(QString::number(lm.getMinor(index, 0)));
    ui->lineEditDelta2 -> setText(QString::number(lm.getMinor(index, 1)));

    //changing indexes of delta labels
    ui->labelDelta1 -> setText("Δ(" + QString::number(index+1) + ", 2) = ");
    ui->labelDelta2 -> setText("Δ(" + QString::number(index+1) + ", 3) = ");

    //showing type of extremum point and max/min/nothing
    switch( lm.getExtremum(index) ) {

        case MIN:
            ui->labelTypeOfPoint->setText("Точка мінімума");
            ui->labelF->setText("min F = ");
            ui->lineEditF->setText(QString::number( lm.getMinimum() ));
            break;
        case MAX:
            ui->labelTypeOfPoint->setText("Точка максимума");
            ui->labelF->setText("max F = ");
            ui->lineEditF->setText(QString::number( lm.getMaximum() ));
            break;
        case INFLECTION:
            ui->labelTypeOfPoint->setText("Точка перегину");
            ui->labelF->setText(" - ");
            ui->lineEditF->setText(" - ");
            break;
    }

    //showing current hessian matrix
    matrix hessian = lm.getHessian(index);
    for(int i=0; i<3; i++) {

        for(int j=0; j<3; j++) {


            QTableWidgetItem *item = new QTableWidgetItem;
            item->setText(QString::number( hessian[i][j] ));
            ui->tableHessian->setItem(i, j, item);
        }
    }

}
