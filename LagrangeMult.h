#include <iostream>
#include <vector>
#include <utility>

using namespace std;
typedef vector<vector<double>> matrix;
enum extremum { MIN, MAX, INFLECTION };

struct root {

	double lambda;
	double x;
	double y;

	public:
		root();
		root(double lambda, double x, double y);
		~root() {};
		friend std::ostream& operator<<(std::ostream& out, const root z);
};

class LagrangeMult {

	private:
		
		static const int SIZE;
		double a;									// coefficients a, b, c
		double b;
		double c;
		double maxF;
		double minF;
		vector<pair<root,extremum>> rootOf;			// vector of 2 roots of system
		matrix minors;								// matrix of minors values;
		vector<matrix> hessians;					// vector of Hessian matricies;		  
		bool hasCalculated;

		double det(matrix) const;					// calculate determinant (only 2 and 3 orders)
		matrix initHessian(const root r);
		double calcMinore(matrix&, bool);
		void checkExtremum(int i);
		void solveSystem();
		void solve();

	public:
		
		LagrangeMult();
		LagrangeMult(double a, double b, double c);
		~LagrangeMult() {};
					
		double getA() const;			//get methods
		double getB() const;
		double getC() const;
		double getMinor(int i, int j);
		const root getRoot(int i);
		const extremum getExtremum(int i);
		const matrix & getHessian(int i);
		double getMaximum();
		double getMinimum();
		
		void setA(double a);			//set methods
		void setB(double b);
		void setC(double c);
		void setCoefficients(double a = 0, double b = 0, double c = 0);
};